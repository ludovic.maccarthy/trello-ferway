import { render, screen } from '@testing-library/react';
import Header from './Header.component';
import * as helper from '../../helper/helper';

describe('Header component', () => {
    test('should render correctly', () => {
        render(helper.wrapWithProvider(<Header />));
        const header = screen.getByTestId('header');
        const logo = screen.getByTestId('trello-logo');
        expect(header).toBeInTheDocument();
        expect(logo).toBeInTheDocument();
    });
});
