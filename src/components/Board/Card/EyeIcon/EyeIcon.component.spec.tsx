import { render, screen } from '@testing-library/react';
import EyeIcon from './EyeIcon.component';
import * as helper from '../../../../helper/helper';

describe('EyeIcon component', () => {
    beforeEach(() => {
        render(helper.wrapWithProvider(<EyeIcon />));
    });
    test('should render correctly', () => {
        const eyeIcon = screen.getByTestId('eye-icon');
        expect(eyeIcon).toBeInTheDocument();
    });
});
