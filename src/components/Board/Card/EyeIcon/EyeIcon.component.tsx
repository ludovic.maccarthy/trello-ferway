import React from 'react';
import './EyeIcon.component.css';

const EyeIcon = () => {
    return <span data-testid="eye-icon" className="eye-icon"></span>;
};

export default EyeIcon;
