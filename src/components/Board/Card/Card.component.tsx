import { useDispatch } from 'react-redux';
import { setCardToModal, showModal } from '../../../features/modal/modalSlice';
import { CardType, List } from '../BoardList/BoardList.component';
import './Card.component.css';
import EyeIcon from './EyeIcon/EyeIcon.component';
import FollowIcon from './FollowIcon/FollowIcon.component';

export interface CardProps {
    card: CardType;
    list: List;
}

const Card = (props: CardProps) => {
    const dispatch = useDispatch();
    const openCard = () => {
        dispatch(setCardToModal({ cardId: props.card.id, list: props.list }));
        dispatch(showModal());
    };

    return (
        <div className="board__card-container" data-testid="board__card-container">
            <button className="board__card-main" type="submit" onClick={openCard}>
                <h2 className="board__card-title">{props.card.title}</h2>
                <div className="board__card-icons">
                    {props.card.followed && <EyeIcon />}
                    {props.card.description !== '' && <FollowIcon />}
                </div>
            </button>
        </div>
    );
};

export default Card;
