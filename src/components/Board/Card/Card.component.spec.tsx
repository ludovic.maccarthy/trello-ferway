import { render, screen } from '@testing-library/react';
import { CardType, List } from '../BoardList/BoardList.component';
import Card from './Card.component';
import * as helper from '../../../helper/helper';

const mockCard: CardType = {
    title: 'test',
    description: 'test',
    id: 1,
    followed: false,
};

const mockList: List = {
    id: 0,
    cards: [],
    title: '',
};

describe('Card component', () => {
    test('should render correcly', async () => {
        render(helper.wrapWithProvider(<Card card={mockCard} list={mockList} />));
        const card = screen.getByTestId('board__card-container');
        expect(card).toBeInTheDocument();
    });
});
