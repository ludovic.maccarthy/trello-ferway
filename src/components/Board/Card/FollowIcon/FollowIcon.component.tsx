import './FollowIcon.component.css';

const FollowIcon = () => {
    return <span data-testid="follow-icon" className="follow-icon"></span>;
};

export default FollowIcon;
