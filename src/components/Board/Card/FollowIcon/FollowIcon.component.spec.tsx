import { render, screen } from '@testing-library/react';
import FollowIcon from './FollowIcon.component';
import * as helper from '../../../../helper/helper';

describe('FollowIcon component', () => {
    beforeEach(() => {
        render(helper.wrapWithProvider(<FollowIcon />));
    });
    test('should render correctly', () => {
        const followIcon = screen.getByTestId('follow-icon');
        expect(followIcon).toBeInTheDocument();
    });
});
