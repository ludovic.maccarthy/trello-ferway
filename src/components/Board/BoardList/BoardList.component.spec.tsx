import { act, render, screen } from '@testing-library/react';
import BoardList from './BoardList.component';
import * as helper from '../../../helper/helper';

describe('BoardList component', () => {
    test('should render correcly', () => {
        act(() => {
            render(helper.wrapWithProvider(<BoardList title={'Title test'} id={0} cards={[]} />));
        });
        const boardListContainer = screen.getByTestId('board__list-container');
        expect(boardListContainer).toBeInTheDocument();
        expect(screen.getByTestId('board__list')).toBeInTheDocument();
    });
});
