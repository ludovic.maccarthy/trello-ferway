import { useDispatch } from 'react-redux';
import { removeList } from '../../../features/lists/listSlice';
import AddCardBtn from '../AddCardBtn/AddCardBtn.component';
import Card from '../Card/Card.component';
import './BoardList.component.css';

export interface CardType {
    title: string;
    description: string;
    followed: boolean;
    id: number;
}

export interface List {
    id: number;
    title: string;
    cards: CardType[];
}

const BoardList = (props: List) => {
    const dispatch = useDispatch();
    const handleDeleteList = (listName: string) => {
        const text = `Vous allez supprimer la liste nommee ${listName}\n Appuyez sur "OK" pour continuer.\n Ou sur "Annuler" pour fermer.`;
        if (window.confirm(text)) {
            dispatch(removeList({ id: props.id }));
        }
    };
    return (
        <div className="board__list-container" data-testid="board__list-container">
            <div className="board__list" data-testid="board__list">
                <div className="board__list-header">
                    <div className="board__list-header-title">{props.title}</div>
                    <button
                        className="board__list-header__elypsis"
                        title="Supprimer cette liste"
                        onClick={() => handleDeleteList(props.title)}
                    >
                        <span className="board__list-header__elypsis-ico"></span>
                    </button>
                </div>
                {props.cards.length ? (
                    <div className="board__list-cards" data-testid="board__list-cards">
                        {props.cards.map((card, i) => {
                            return <Card card={card} key={card.id + i} list={props} />;
                        })}
                    </div>
                ) : null}
                <AddCardBtn listId={props.id} />
            </div>
        </div>
    );
};

export default BoardList;
