import { render, screen } from '@testing-library/react';
import Board from './Board.component';
import * as helper from '../../helper/helper';

describe('Board component', () => {
    beforeEach(() => {
        render(helper.wrapWithProvider(<Board />));
    });
    test('should render correctly', () => {
        const board = screen.getByTestId('board');
        expect(board).toBeInTheDocument();
    });

    test('BoardHeader should be displayed', () => {
        const boardHeader = screen.getByTestId('board__header');
        expect(boardHeader).toBeInTheDocument();
    });

    test('BoardBody should be displayed', () => {
        const boardBody = screen.getByTestId('board__body');
        expect(boardBody).toBeInTheDocument();
    });
});
