import './Board.component.css';
import BoardBody from './BoardBody/BoardBody.component';
import BoardHeader from './BoardHeader/BoardHeader.component';

const Board = () => {
    return (
        <div data-testid="board">
            <BoardHeader />
            <BoardBody />
        </div>
    );
};

export default Board;
