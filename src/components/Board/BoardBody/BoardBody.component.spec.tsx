import { act, render, screen } from '@testing-library/react';
import BoardBody from './BoardBody.component';
import * as helper from '../../../helper/helper';

describe('BoardBody component', () => {
    test('should render correcly', async () => {
        act(() => {
            render(helper.wrapWithProvider(<BoardBody />));
        });
        const boardBody = screen.getByTestId('board__body');
        expect(boardBody).toBeInTheDocument();
    });
});
