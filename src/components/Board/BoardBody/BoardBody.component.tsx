import { useEffect } from 'react';
import './BoardBody.component.css';
import BoardList, { List } from '../BoardList/BoardList.component';
import { useDispatch, useSelector } from 'react-redux';
import { fetchLists, selectLists } from '../../../features/lists/listSlice';
import AddListBtn from '../AddListBtn/AddListBtn.component';

const BoardBody = () => {
    const dispatch = useDispatch();
    const lists = useSelector(selectLists);

    useEffect(() => {
        dispatch(fetchLists());
    }, [dispatch]);

    return (
        <div className="board__body" data-testid="board__body">
            {lists.lists &&
                lists.lists.map((liste: List, i) => {
                    return <BoardList key={liste.title + i} {...liste} />;
                })}
            <AddListBtn />
        </div>
    );
};

export default BoardBody;
