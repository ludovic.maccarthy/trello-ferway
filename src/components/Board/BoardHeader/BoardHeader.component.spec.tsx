import { fireEvent, render, screen } from '@testing-library/react';
import BoardHeader from './BoardHeader.component';
import * as helper from '../../../helper/helper';

describe('BoardHeader component', () => {
    test('should render correcly', async () => {
        render(helper.wrapWithProvider(<BoardHeader />));
        const boardHeader = screen.getByTestId('board__header');
        const initializeDataBtn = screen.getByTestId('board__header-init_btn');
        expect(boardHeader).toBeInTheDocument();
        expect(initializeDataBtn).toBeInTheDocument();
        fireEvent.click(initializeDataBtn);
    });
});
