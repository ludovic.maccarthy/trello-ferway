import { useDispatch } from 'react-redux';
import { resetLists } from '../../../features/lists/listSlice';
import './BoardHeader.component.css';

const BoardHeader = () => {
    const dispatch = useDispatch();
    const onInitializeDatas = () => {
        dispatch(resetLists());
    };
    return (
        <div data-testid="board__header" className="board__header">
            <h1 className="board__header-title" data-testid="board__header-title">
                Tableau principal
            </h1>
            <button
                className="board__header-init_btn"
                data-testid="board__header-init_btn"
                type="submit"
                onClick={() => onInitializeDatas()}
            >
                Initialiser le jeu de données
            </button>
        </div>
    );
};

export default BoardHeader;
