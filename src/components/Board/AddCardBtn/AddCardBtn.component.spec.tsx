import { act, render, screen } from '@testing-library/react';
import AddCardBtn from './AddCardBtn.component';
import * as helper from '../../../helper/helper';

describe('AddCardBtn component', () => {
    beforeEach(() => {
        act(() => {
            render(helper.wrapWithProvider(<AddCardBtn listId={1} />));
        });
    });
    test('should render correctly', async () => {
        await screen.findByTestId('board__add-card-principal');
        expect(screen.getByTestId('board__add-card-principal')).toBeInTheDocument();
    });
});
