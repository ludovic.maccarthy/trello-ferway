import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addCardToList } from '../../../features/lists/listSlice';
import IconDelete from '../../Icons/IconDelete.component';
import IconPlus from '../../Icons/IconPlus.component';
import './AddCardBtn.component.css';

export interface AddCardBtnProps {
    listId: number;
}

const AddCardBtn = (props: AddCardBtnProps) => {
    const dispatch = useDispatch();
    const [addCardOpen, setAddCardOpen] = useState<boolean>(false);
    const [cardName, setCardName] = useState('');
    const addCardRef = useRef(null);

    useEffect(() => {
        if (addCardOpen) {
            document.querySelector('textarea')?.focus();
        }
    });

    useEffect(() => {
        const handle = (e: MouseEvent) => {
            if (addCardRef && addCardRef.current && addCardRef?.current) {
                let current = addCardRef.current as HTMLDivElement;
                if (!current.contains(e.target as HTMLElement)) {
                    setAddCardOpen(false);
                }
            }
        };
        document.addEventListener('mousedown', handle);
        return () => {
            document.removeEventListener('mousedown', handle);
        };
    }, [addCardRef]);

    const onAddCard = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        setAddCardOpen(true);
    };

    const onCloseAddCard = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        setAddCardOpen(false);
    };

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (cardName !== '') {
            dispatch(addCardToList({ title: cardName, id: props.listId }));
            setCardName('');
            setAddCardOpen(false);
        }
    };

    const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const textValue: string = e.target.value;
        setCardName(textValue);
    };

    return (
        <div className="board__add-card-principal" data-testid="board__add-card-principal" ref={addCardRef}>
            {!addCardOpen ? (
                <form className="board__add-card-container" onSubmit={(e) => onSubmit(e)}>
                    <button className="board__add-card-main" type="submit" onClick={(e) => onAddCard(e)}>
                        <IconPlus />
                        Ajouter une autre carte
                    </button>
                </form>
            ) : (
                <form className="board__add-card-container" onSubmit={(e) => onSubmit(e)}>
                    <div>
                        <textarea
                            className="board__add-card__textarea"
                            placeholder="Saisissez un titre pour cette carte…"
                            onChange={(e) => handleChange(e)}
                        ></textarea>
                        <div className="board__add-card__actions">
                            <button className="board__add-card__actions-add" type="submit">
                                Ajouter une carte
                            </button>
                            <button
                                className="board__add-card__actions-close"
                                type="submit"
                                onClick={(e) => onCloseAddCard(e)}
                            >
                                <IconDelete />
                            </button>
                        </div>
                    </div>
                </form>
            )}
        </div>
    );
};

export default AddCardBtn;
