import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addListToBoard } from '../../../features/lists/listSlice';
import IconDelete from '../../Icons/IconDelete.component';
import './AddListBtn.component.css';

const AddListBtn = () => {
    const dispatch = useDispatch();
    const [addListOpen, setAddListOpen] = useState<boolean>(false);
    const [listName, setListName] = useState('');
    const addListRef = useRef(null);

    const onCloseAddList = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        setAddListOpen(false);
    };

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (listName !== '') {
            dispatch(addListToBoard({ title: listName }));
            setListName('');
            setAddListOpen(false);
        }
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const textValue: string = e.target.value;
        setListName(textValue);
    };

    const handleAddList = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        setAddListOpen(true);
    };

    useEffect(() => {
        const handle = (e: MouseEvent) => {
            if (addListRef && addListRef.current && addListRef?.current) {
                let current = addListRef.current as HTMLDivElement;
                if (!current.contains(e.target as HTMLElement)) {
                    setAddListOpen(false);
                }
            }
        };
        // add the event listener
        document.addEventListener('mousedown', handle);
        return () => {
            document.removeEventListener('mousedown', handle);
        };
    }, [addListRef]);

    return (
        <div className="board__list-container" ref={addListRef} tabIndex={0}>
            {!addListOpen ? (
                <form className="board__add-list-form-1">
                    <button className="board__add-list-btn" type="submit" onClick={(e) => handleAddList(e)}>
                        <span className="icon-plus"></span>Ajouter une autre liste
                    </button>
                </form>
            ) : (
                <form className="board__add-list-form-2" onSubmit={onSubmit}>
                    <div>
                        <input
                            className="board__add-list-input"
                            type="text"
                            placeholder="Saisissez le titre de la liste…"
                            onChange={handleChange}
                        />
                        <div className="board__add-list__actions">
                            <button className="board__add-list__submit-btn" type="submit">
                                Ajouter une liste
                            </button>
                            <button className="board__add-list__cancel-btn" type="submit" onClick={onCloseAddList}>
                                <IconDelete />
                            </button>
                        </div>
                    </div>
                </form>
            )}
        </div>
    );
};

export default AddListBtn;
