import { act, render, screen } from '@testing-library/react';
import DescriptionArea from './DescriptionArea.component';
import * as helper from '../../helper/helper';

describe('DescriptionArea component', () => {
    beforeEach(() => {
        const card = helper.buildCards(1);
        card[0].description = 'Description';
        act(() => {
            render(helper.wrapWithProvider(<DescriptionArea card={card[0]} />));
        });
    });
    test('should render correctly', async () => {
        await screen.findByTestId('desc-area');
        expect(screen.getByTestId('desc-area')).toBeInTheDocument();
    });
});
