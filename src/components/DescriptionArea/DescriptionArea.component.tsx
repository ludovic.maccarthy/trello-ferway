import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateDescription } from '../../features/lists/listSlice';
import { selectModal } from '../../features/modal/modalSlice';
import { CardType } from '../Board/BoardList/BoardList.component';
import IconDelete from '../Icons/IconDelete.component';
import './DescriptionArea.component.css';

export interface DescriptionAreaProps {
    card: CardType;
}

const DescriptionArea = (props: DescriptionAreaProps) => {
    const modalFromStore = useSelector(selectModal);
    const dispatch = useDispatch();
    const [showEditDesc, setShowEditDesc] = useState(false);
    const [description, setDescription] = useState('');
    const wrapperRef = useRef(null);

    const onClickDesc = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        setShowEditDesc(true);
    };

    const handleAddDescription = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        if (description !== '') {
            dispatch(
                updateDescription({
                    card: props.card,
                    list: modalFromStore.list,
                    description,
                })
            );
            setShowEditDesc(false);
        }
    };

    const handleCloseAddDesc = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        setShowEditDesc(false);
    };

    useEffect(() => {
        const handle = (e: MouseEvent) => {
            if (wrapperRef && wrapperRef.current && wrapperRef?.current) {
                let current = wrapperRef.current as HTMLDivElement;
                if (!current.contains(e.target as HTMLElement)) {
                    setShowEditDesc(false);
                }
            }
        };
        // add the event listener
        document.addEventListener('mousedown', handle);
        return () => {
            document.removeEventListener('mousedown', handle);
        };
    }, [wrapperRef]);

    return (
        <div ref={wrapperRef} data-testid="desc-area">
            {showEditDesc ? (
                <div>
                    <form className="form-add-desc">
                        <textarea
                            className="form-add-desc_textarea"
                            name="cardDesc"
                            placeholder="Ajouter une description plus détaillée…"
                            onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                        <div className="form-add-description-actions">
                            <button className="form-add-description-btn" type="submit" onClick={handleAddDescription}>
                                Enregistrer
                            </button>
                            <button
                                className="form-add-description-cancel-btn"
                                type="submit"
                                onClick={handleCloseAddDesc}
                            >
                                <IconDelete />
                            </button>
                        </div>
                    </form>
                </div>
            ) : props.card.description !== '' ? (
                <div className="description" onClick={onClickDesc}>
                    {props.card.description}
                </div>
            ) : (
                <div className="add-description" onClick={onClickDesc}>
                    Ajouter une description plus détaillée…
                </div>
            )}
        </div>
    );
};

export default DescriptionArea;
