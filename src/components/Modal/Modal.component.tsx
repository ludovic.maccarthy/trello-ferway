import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCard, followCard, selectLists } from '../../features/lists/listSlice';
import { hideModal, selectModal } from '../../features/modal/modalSlice';
import { CardType, List } from '../Board/BoardList/BoardList.component';
import EyeIcon from '../Board/Card/EyeIcon/EyeIcon.component';
import DescriptionArea from '../DescriptionArea/DescriptionArea.component';
import './Modal.component.css';

const Modal = () => {
    const dispatch = useDispatch();
    const modalFromStore = useSelector(selectModal);
    const listsFromStore = useSelector(selectLists);

    const [card, setCard] = useState<CardType>({ description: '', id: 0, title: '', followed: false });

    const closeModal = () => {
        dispatch(hideModal());
    };

    const handleFollow = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        if (modalFromStore && modalFromStore.list && modalFromStore.list.cards) {
            const follow =
                modalFromStore.list.cards.filter((c: CardType) => {
                    return c.id === card.id;
                }).length > 1
                    ? true
                    : false;

            dispatch(
                followCard({
                    listId: modalFromStore.list.id,
                    cardId: card.id,
                    follow,
                })
            );
        }
    };

    const handleDelete = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();

        const text = `Vous allez supprime la carte nommee ${card.title}. \nAppuyez sur "OK" pour continuer. \nOu sur "Annuler" pour fermer.`;
        if (window.confirm(text)) {
            dispatch(
                deleteCard({
                    listId: modalFromStore.list.id,
                    cardId: card.id,
                })
            );
            dispatch(hideModal());
        }
    };

    useEffect(() => {
        if (listsFromStore && listsFromStore.lists) {
            const list = listsFromStore.lists.filter((list: List) => {
                return list.id === modalFromStore.list.id;
            });
            if (list.length) {
                const cardFound: CardType[] = list[0].cards.filter((card: CardType) => {
                    return card.id === modalFromStore.cardId;
                });

                if (cardFound.length && JSON.stringify(cardFound[0]) !== JSON.stringify(card)) {
                    setCard({ ...cardFound[0] });
                }
            }
        }
    }, [listsFromStore.lists, modalFromStore.list, modalFromStore.cardId, card, listsFromStore]);

    return (
        <div className="modal" data-testid="modal">
            <div className="modal__backdrop" data-testid="modal__backdrop" onClick={closeModal}></div>
            <div className="modal__main" data-testid="modal__main">
                <div className="modal__container">
                    <div className="modal__content">
                        <span className="close-modal-btn" onClick={closeModal}></span>
                        <div className="modal__content-body">
                            <div className="modal__content-top">
                                <h3 className="modal__content-title">{card.title}</h3>
                                <div className="modal__content-from-list">
                                    <span className="modal__content-from-list-desc">
                                        Dans la liste{'  '}
                                        <span className="modal__content-from-list-title">
                                            {modalFromStore.list.title}
                                        </span>
                                        {card.followed && <EyeIcon />}
                                    </span>
                                </div>
                            </div>
                            <div className="modal__content-bottom">
                                <div className="modal__content-bottom__left">
                                    <div className="modal__content-bottom__left-title">Description</div>
                                    <DescriptionArea card={card} />
                                </div>
                                <div className="modal__content-bottom__right">
                                    <div className="modal__content-bottom__right-title">Actions</div>
                                    <button className="modal__content-follow-btn" type="submit" onClick={handleFollow}>
                                        <EyeIcon />
                                        Suivre
                                        {card.followed && (
                                            <span className="modal__content-followed-container">
                                                <span className="modal__content-followed-ico"></span>
                                            </span>
                                        )}
                                    </button>
                                    <button
                                        className="modal__content-delete-btn modal__content-follow-btn"
                                        type="submit"
                                        onClick={handleDelete}
                                    >
                                        <span className="modal__content-delete-ico"></span>Supprimer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Modal;
