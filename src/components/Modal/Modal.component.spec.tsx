import { act, render, screen } from '@testing-library/react';
import Modal from './Modal.component';
import * as helper from '../../helper/helper';

describe('Modal component', () => {
    beforeEach(() => {
        act(() => {
            render(helper.wrapWithProvider(<Modal />));
        });
    });
    test('should render correctly', () => {
        const modal = screen.getByTestId('modal');
        expect(modal).toBeInTheDocument();
    });
});
