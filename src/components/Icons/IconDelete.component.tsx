import './IconDelete.component.css';

const IconDelete = () => {
    return <span className="delete-icon" data-testid="delete-icon"></span>;
};

export default IconDelete;
