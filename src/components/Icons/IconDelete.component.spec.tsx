import { render, screen } from '@testing-library/react';
import IconDelete from './IconDelete.component';
import * as helper from '../../helper/helper';

describe('IconDelete component', () => {
    beforeEach(() => {
        render(helper.wrapWithProvider(<IconDelete />));
    });
    test('should render correctly', () => {
        const deleteIcon = screen.getByTestId('delete-icon');
        expect(deleteIcon).toBeInTheDocument();
    });
});
