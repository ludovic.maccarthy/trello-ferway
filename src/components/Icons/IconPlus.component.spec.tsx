import { render, screen } from '@testing-library/react';
import IconPlus from './IconPlus.component';
import * as helper from '../../helper/helper';

describe('IconPlus component', () => {
    beforeEach(() => {
        render(helper.wrapWithProvider(<IconPlus />));
    });
    test('should render correctly', () => {
        const plusIcon = screen.getByTestId('board__add-card-ico');
        expect(plusIcon).toBeInTheDocument();
    });
});
