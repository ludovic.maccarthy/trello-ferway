import React from 'react';
import './IconPlus.component.css';

const IconPlus = () => {
    return <span className="board__add-card-ico" data-testid="board__add-card-ico"></span>;
};

export default IconPlus;
