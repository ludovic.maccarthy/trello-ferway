import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { List } from '../../components/Board/BoardList/BoardList.component';
import { datas } from '../../datas/data';

const initialState: ListsState = {
    lists: [],
};

export interface ListsState {
    lists: List[];
}

export const listsSlice = createSlice({
    name: 'lists',
    initialState,
    reducers: {
        fetchLists: (state) => {
            state.lists = datas;
        },
        resetLists: (state) => {
            state.lists = datas;
        },
        addList: (state, action) => {
            state.lists = [...state.lists, action.payload];
        },
        removeList: (state, action) => {
            state.lists = state.lists.filter((list: List) => list.id !== action.payload.id);
        },
        addCardToList: (state, action) => {
            const list = state.lists.filter((list) => list.id === action.payload.id)[0];
            const card = {
                id: Date.now(),
                title: action.payload.title,
                description: '',
                followed: false,
            };
            state.lists.filter((list) => list.id === action.payload.id)[0].cards = [...list.cards, card];
        },
        addListToBoard: (state, action) => {
            const list: List = {
                title: action.payload.title,
                cards: [],
                id: Date.now(),
            };
            state.lists = [...state.lists, list];
        },
        followCard: (state, action) => {
            state.lists.forEach((liste) => {
                if (liste.id === action.payload.listId) {
                    liste.cards.forEach((card) => {
                        if (card.id === action.payload.cardId) {
                            card.followed = !card.followed;
                        }
                    });
                }
            });
        },
        deleteCard: (state, action) => {
            state.lists.forEach((list) => {
                if (list.id === action.payload.listId) {
                    list.cards = list.cards.filter((card) => card.id !== action.payload.cardId);
                }
            });
        },
        updateDescription: (state, action) => {
            const listId = action.payload.list.id;
            const list = state.lists.filter((list) => listId === list.id);
            if (list.length) {
                list[0].cards.forEach((card) => {
                    if (card.id === action.payload.card.id) {
                        card.description = action.payload.description;
                    }
                });
            }
        },
    },
});

export const {
    fetchLists,
    resetLists,
    addList,
    removeList,
    addCardToList,
    addListToBoard,
    followCard,
    deleteCard,
    updateDescription,
} = listsSlice.actions;

export const selectLists = (state: RootState) => state.lists;

export default listsSlice.reducer;
