import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { List } from '../../components/Board/BoardList/BoardList.component';

export interface ListsState {
    visible: boolean;
    cardId: number;
    list: List;
}

const initialState: ListsState = {
    visible: false,
    cardId: 0,
    list: {
        id: 0,
        title: '',
        cards: [],
    },
};

export const modalSlice = createSlice({
    name: 'lists',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        showModal: (state) => {
            state.visible = true;
        },
        hideModal: (state) => {
            state.visible = false;
        },
        setCardToModal: (state, action) => {
            state.cardId = action.payload.cardId;
            state.list = action.payload.list;
        },
    },
});

export const { showModal, hideModal, setCardToModal } = modalSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectModal = (state: RootState) => state.modal;

export default modalSlice.reducer;
