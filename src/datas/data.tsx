export const datas = [
    {
        id: 1,
        title: 'First list',
        cards: [
            {
                id: 1,
                title: 'My first card',
                description: '',
                followed: false,
            },
            {
                id: 2,
                title: 'My second card',
                description: '',
                followed: false,
            },
            {
                id: 3,
                title: 'Followed card',
                description: '',
                followed: true,
            },
        ],
    },
    {
        id: 2,
        title: 'My second list',
        cards: [
            {
                id: 4,
                title: 'Followed card with description',
                description: 'Description',
                followed: true,
            },
        ],
    },
];
