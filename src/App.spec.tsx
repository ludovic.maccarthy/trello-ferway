import { render, screen } from '@testing-library/react';
import App from './App';
import * as helper from './helper/helper';

describe('App component', () => {
    beforeEach(() => {
        render(helper.wrapWithProvider(<App />));
    });
    test('should render correctly', () => {
        const app = screen.getByTestId('App');
        expect(app).toBeInTheDocument();
    });

    test('Header should be displayed', () => {
        const header = screen.getByTestId('header');
        expect(header).toBeInTheDocument();
    });

    test('Board should be displayed', () => {
        const board = screen.getByTestId('board');
        expect(board).toBeInTheDocument();
    });
});
