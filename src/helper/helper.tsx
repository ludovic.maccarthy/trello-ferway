import configureStore from 'redux-mock-store';
import { ReactElement } from 'react';
import { Provider } from 'react-redux';
import { datas } from '../datas/data';
import { CardType } from '../components/Board/BoardList/BoardList.component';

export const buildCards = (count: number): CardType[] => {
    let i = 0;
    const cards = [];
    for (i === 0; i < count; i++) {
        const initialCardState: CardType = {
            id: 0 + i,
            title: 'Pokemon ' + i,
            description: 'description ' + i,
            followed: true,
        };
        cards.push(initialCardState);
    }
    return cards;
};

const initialModalState = {
    visible: false,
    cardId: 1,
    list: {
        id: 1,
        title: 'Test title',
        cards: buildCards(20),
    },
};

const defaultStore = {
    lists: datas,
    modal: initialModalState,
};

export const wrapWithProvider = (child: ReactElement): JSX.Element => {
    return <Provider store={mockedStore()}>{child}</Provider>;
};

export const mockedStore = () => {
    const mockStore = configureStore();
    const store = mockStore(defaultStore);
    return store;
};
