import { useSelector } from 'react-redux';
import './App.css';
import Board from './components/Board/Board.component';
import Header from './components/Header/Header.component';
import Modal from './components/Modal/Modal.component';
import { selectModal } from './features/modal/modalSlice';

function App() {
    const modalFromStore = useSelector(selectModal);

    return (
        <div className="App" data-testid="App">
            <Header />
            <Board />
            {modalFromStore.visible && <Modal />}
        </div>
    );
}

export default App;
